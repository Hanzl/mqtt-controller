import machine
import dht
import ds18x20
import onewire
import utime
import errno


class HumidityDthSensor:

    def __init__(self):
        self.sensor_pin_num = 4
        self.dht_sensor = None
        self.prev_humidity = None
        self.prev_temperature = None

    def setup(self):
        dth_sensor_pin = machine.Pin(self.sensor_pin_num)
        self.dht_sensor = dht.DHT11(dth_sensor_pin)
        print(f'HumidityDthSensor: Found following devices: {self.dht_sensor}')

    def measure(self):
        try:
            self.dht_sensor.measure()
            temperature = self.dht_sensor.temperature()
            humidity = self.dht_sensor.humidity()
            self.prev_temperature = temperature
            self.prev_humidity = humidity
            return temperature, humidity
        except OSError as e:
            if e.args[0] == errno.ETIMEDOUT:
                return self.prev_temperature, self.prev_humidity
            else:
                print("HumidityDthSensor: Error reading sensor OSERROR request:", e)
                return None, None
        except Exception as e:
            print("HumidityDthSensor: Error reading sensor request:", e)
            return None, None


class TemperatureSensor:
    def __init__(self):
        self.sensor_pin_num = 5
        self.sensor_bus = None
        self.temperature_sensors = None

    def setup(self):
        temperature_sensor_pin = machine.Pin(self.sensor_pin_num)
        self.sensor_bus = ds18x20.DS18X20(onewire.OneWire(temperature_sensor_pin))
        self.temperature_sensors = self.sensor_bus.scan()
        print(f'TemperatureSensor: Found following devices: {self.temperature_sensors}')

    def measure(self):
        try:
            self.sensor_bus.convert_temp()
            temperature = self.sensor_bus.read_temp(self.temperature_sensors[0])
            return temperature

        except Exception as e:
            print("TemperatureSensor: Error reading sensor request:", e)
            return None
