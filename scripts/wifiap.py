import network
import binascii
import utime
import ubinascii
try:
    import usocket as socket
except ImportError:
    import socket


class WiFiController:
    WAIT_TO_CONNECT_TIME_SEC = 5
    WAIT_FOR_MANUAL_CONNECT_TIME_SEC = 10 * 60

    def __init__(self):
        self.ap_ssid = "Sensor-AP-Red"
        self.ap_password = "12345678"
        self.ap_if = None
        self.ap_socket = None
        self.ap_socket_port = 80

        self.sta_if = None
        self.scanned_wifi_data = None
        self.known_ssids_and_passwords = {
            "name": "password",
            "Test1": "BushDid*911"
        }

    def connect(self):

        self.ap_if = network.WLAN(network.AP_IF)
        self.ap_if.active(False)
        connected = self.connect_automatically()
        print("Connection status: ", connected)
        # if not connected:
        # utime.sleep(self.WAIT_TO_CONNECT_TIME_SEC)
        # self.connect_with_ap()

    def connect_automatically(self):
        self.sta_if = network.WLAN(network.STA_IF)
        self.sta_if.active(True)
        self.scanned_wifi_data = self.sta_if.scan()
        if self.scanned_wifi_data is None:
            print("No network found!")
            return False
        for (ssid, bssid, channel, RSSI, security, hidden) in self.scanned_wifi_data:
            ssid = ssid.decode()
            print(f'Found following devices: {ssid, bssid, channel, RSSI, security, hidden}')
            # Connect to a known WiFi or Connect to an open access point
            if ssid in self.known_ssids_and_passwords.keys() or binascii.hexlify(bssid) == 0:
                print(f"Connecting to WiFi: {ssid}")
                self.sta_if.active(True)
                self.sta_if.connect(ssid, self.known_ssids_and_passwords[ssid])
                utime.sleep(self.WAIT_TO_CONNECT_TIME_SEC)
                break
        return self.sta_if.isconnected()

    def connect_with_ap(self):
        try:
            print("Connect with wifi access point")
            self.setup_ap()
            self.setup_socket()
            start_time = utime.time()
            while start_time < utime.time() + self.WAIT_FOR_MANUAL_CONNECT_TIME_SEC:
                print("WiFi AP: Staring a new cycle...")
                conn, addr = self.ap_socket.accept()
                print(f'Received a connection from {addr}')
                request = conn.recv(1024)
                request = request.decode("utf-8")
                print(f"Received request: {request}")
                ssid, password = self.parse_data_received(request)
                if ssid and password:
                    print("Correctly formatted response")
                    response = load_html_file('connected.html')
                    conn.write(response)
                    conn.close()
                    break
                response = load_html_file('loginPage.html')
                conn.write(response)
                conn.close()
                utime.sleep(self.WAIT_TO_CONNECT_TIME_SEC)
            else:
                return False

            self.ap_if.active(False)
            print(f"Setting up sta_if with: {ssid, password}")
            if not self.sta_if:
                self.sta_if = network.WLAN(network.STA_IF)
                self.sta_if.active(True)
            print("Connection to wifi")
            self.sta_if.connect(ssid, password)
            utime.sleep(self.WAIT_TO_CONNECT_TIME_SEC)
            return self.sta_if.isconnected()

        except Exception as exp:
            print(exp)
            return False

    def setup_ap(self):
        self.ap_if = network.WLAN(network.AP_IF)
        self.ap_if.active(True)
        self.ap_if.config(essid=self.ap_ssid, password=self.ap_password)
        print(f'Setup AP: {self.ap_if.ifconfig()}')
        return self.ap_if.isconnected()

    def setup_socket(self):
        self.ap_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ap_socket.bind(('', self.ap_socket_port))
        self.ap_socket.listen()

    def parse_data_received(self, request):
        if 'password' in request and 'ssid' in request:
            request = request.replace('?', '#')
            request = request.replace('=', '#')
            request = request.replace('&', '#')
            split_data = request.split('#')
            ssid = split_data[split_data.index('ssid') + 1]
            password = split_data[split_data.index('password') + 1]

            print(f"{ssid, password}")
            if self.scanned_wifi_data:
                if not any([_ssid.decode() == ssid for (_ssid, bssid, channel, RSSI, security, hidden) in self.scanned_wifi_data]):
                    print(f"Ssid: {ssid} not found in scanned wifi networks")
                    return None, None

            if len(ssid) > 0 and len(password) > 0:
                print(f"Valid ssid and password: {ssid, password}")
                return ssid, password
        return None, None

    def is_connected(self):
        if self.sta_if:
            return self.sta_if.isconnected()
        return False


def load_html_file(file_name):
    with open(file_name, 'r') as file:
        return file.read()
