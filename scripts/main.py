import time
import json
import utime
import ubinascii
import network
import machine
from umqtt.simple import MQTTClient
import ntptime
import sensors
import wifiap
import gc

"""
message structure:
{'team_name': string, 'timestamp': string, 'temperature': float, 'humidity': float, 'illumination': float}

"""
DEBUG_MODE = True
WATCHDOG_TIMEOUT_MS = 5000


def timeit(func, name='noname'):
    def timeit_wrapper(*args, **kwargs):
        start = utime.time_ns()
        result = func(*args, **kwargs)
        print(f'{name} | Elapsed time: {(utime.time_ns() - start)/1E6} ms') if DEBUG_MODE else None
        return result
    return timeit_wrapper


class MQTTController:
    SLEEP_TIME_SEC = 1
    BROKER_CYCLE_TIME_SEC = 60

    def __init__(self):
        self.humidity_sensor = sensors.HumidityDthSensor()
        self.temperature_sensor = sensors.TemperatureSensor()
        self.wifi_controller = wifiap.WiFiController()

        # MQTT Broker
        self.mqtt_client = None
        self.client_id = 'esp8266-red'
        self.mqtt_server = '147.228.124.47'
        self.mqtt_port = 1883
        self.mqtt_user = "student"
        self.mqtt_password = "pivotecepomqtt"
        self.topic = 'ite/red'
        self.support_topic = 'ite/red/test'

    def setup_sensors(self):
        self.humidity_sensor.setup()
        self.temperature_sensor.setup()

    def run(self):
        self.setup()
        self.event_loop()

    def setup(self):
        self.setup_sensors()
        gc.collect()
        self.wifi_controller.connect()
        gc.collect()
        self.connect_to_mqtt()

    def set_ntp_time(self):
        for i in range(3):
            try:
                ntptime.settime()
                break
            except Exception as e:
                print("Error setting time:", e)

    def safe_get_time_ns(self):
        try:
            return utime.time_ns()
        except Exception:
            return None

    def safe_get_time(self):
        try:
            return utime.time()
        except Exception:
            return None

    def event_loop(self):
        self.set_ntp_time()
        start_time = self.safe_get_time()
        while True:
            timeit(self.set_ntp_time, "time_sync")()
            for i in range(10):

                # timeit(self.set_ntp_time, "set_ntp_time")()
                cycle_start = self.safe_get_time_ns()
                # lets not divide, it sucks
                current_time_sec = self.safe_get_time()
                temperature = timeit(self.temperature_sensor.measure, "temp")()
                _temp, humidity = timeit(self.humidity_sensor.measure, "hum")()

                if not temperature:
                    temperature = _temp
                if temperature is None or humidity is None:
                    continue
                message_body = timeit(self.create_message_body, "msg")(current_time_sec, temperature, humidity)
                if not message_body:
                    continue

                try:
                    timeit(self.mqtt_client.publish, "publish")(self.support_topic, message_body)
                    if current_time_sec - start_time > self.BROKER_CYCLE_TIME_SEC:
                        self.mqtt_client.publish(self.topic, message_body)
                        start_time = self.safe_get_time()
                except Exception:
                    print("Error publishing message:", message_body)
                gc.collect()
                try:
                    # timeit(self.set_ntp_time, "set_ntp_time2")()
                    timeit(utime.sleep_ms, "sleep")(int(max(self.SLEEP_TIME_SEC * 1E3 - ((self.safe_get_time_ns() - cycle_start) * 1E-6), 0)))
                except Exception:
                    print("Error sleeping:", utime.time_ns() - cycle_start)
                print("-------------------")

    @staticmethod
    def create_message_body(current_time_sec, temperature, humidity):
        try:
            timestamp = utime.localtime(current_time_sec + 1 * 3600)
            timestamp = "{:04d}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}".format(
                timestamp[0], timestamp[1], timestamp[2],
                timestamp[3], timestamp[4], timestamp[5]
            )
            message = {
                "team_name": "red",
                "timestamp": timestamp,
                "temperature": float(temperature),
                "humidity": float(humidity)
            }
            return json.dumps(message)
        except Exception as e:
            print("Error creating message body:", e)
            return None

    def connect_to_mqtt(self):
        self.mqtt_client = MQTTClient(
            client_id=self.client_id,
            server=self.mqtt_server,
            port=self.mqtt_port,
            user=self.mqtt_user,
            password=self.mqtt_password
        )
        self.mqtt_client.connect()
        utime.sleep(5)


def get_mac_address():
    wlan_sta = network.WLAN(network.STA_IF)
    wlan_sta.active(True)
    wlan_mac = wlan_sta.config("mac")
    print(ubinascii.hexlify(wlan_mac).decode())


if __name__ == '__main__':
    restart_wait_time_sec = 10
    gc.disable()

    while True:
        for i in range(10):
            gc.collect()
            try:
                mqtt_controller = MQTTController()
                mqtt_controller.run()
            except Exception as exp:
                print(f'BIG LOOP | Failed with: {exp}')
        # time.sleep(restart_wait_time_sec)
        machine.reset()

"esptool.py --port COM5 erase_flash"
"esptool.py --port COM5 --baud 115200 write_flash --flash_size=detect 0 ./ESP8266_GENERIC-20231005-v1.21.0.bin"
# vm.523034.xyz
